# piafmobinstall

Note: Checkboxen sind eventuell nicht mit Status gerendert (alle "aus/ohne Haken"), daher nutze das [README "Originalformat"](https://codeberg.org/LTZ-Digitalisierung/piafmobinstall/raw/branch/main/README.md).

Installation von piaf.mobile auf mobilen Geräten via adb/USB.

Diese Skripte können auf Androidgeräten (Zebra oder Tablets) bei der Einrichtung und Installation helfen.

Genutzt und getestet auf:
* Zebra MC2700
* Tablets
  * Tab Active2 SM-T395
  * Tab Active Pro SM

Als Hostsystem kommt ein Linux in Frage, da die Skripte mit der bash ausgeführt werden müssen. Installation von adb und imagemagick nötig.
Ungetestet unter Windows, dort eventuell via WSL möglich.

## Vorbereitungen am Gerät

### Initiales Setup eines Android

Beispiel mit frischem Zebra
* WLAN verbinden
    * eventuell Tastatur einblenden (unten rechts) und GBoard als Tastatureingabe einstellen
* Ersteinrichtung
    * Google Account - skip
    * Google services
        * [x] location
        * [x] allow Wifi scan (optional)
        * [ ] send usage and diagnostics data
    * PIN oder Pattern konfigurieren
        * [x] secure startup
    * Gerätenamen setzen
        * settings - about phone - Device name
    * adb management ermöglichen - ADB ist die Android debugging bridge und ermöglicht die Administration via USB, dazu muss das auf dem Gerät eingeschaltet werden:
        * settings - about phone - build number - 7x tippen für Developer Mode/Entwicklermodus
        * settings - System - Developer options 
            * [x] usb debugging (suchen)
* Sprache einstellen (Deutsch)
    * System - Languages & Input - Languages
        * Deutsch hinzufügen und den Eintrag nach oben schieben
* Gerät via USB anschließen
    * meldungen - usb lädt oder 
    * Connected devices - USB Preferences
        * [x] Dateiübertragung, eventuell auch später öfter zu checken, z.B. unter neuem Profil
    * `adb devices` sollte das Gerät erkennen
        * USB debugging popup auf Android
            * [x] remember and allow
```
$ adb devices # kein debugging an
List of devices attached
21296523023568	no permissions (user in plugdev group; are your udev rules wrong?); see [http://developer.android.com/tools/device.html]
$ adb devices # noch nicht am Gerät bestätigt
List of devices attached
21296523023568	unauthorized
$ adb devices # bestätigt
List of devices attached
21296523023568	device
$ adb shell whoami
shell
```

* google account einrichten (optional)

## Installationsdateien

* APKs unter adb-data/ ablegen (werden nicht versioniert, via .gitignore ausgelassen)
    * APKs können von einer manuellen Installation erhalten werden, z.B. mit `android-getapks.sh` Skript
* Hintergrundlogo kann erzeugt werden, dazu ein Template anlegen

Optional: Für Matrix Einrichtung können username und passwort aus [pass](https://www.passwordstore.org/) als QR code angezeigt werden, dafür `piaf.mobile/matrixuser` `piaf.mobile/matrixpw`

### Organicmaps Offline Karten

Für Organicmaps können die Offline-Karten auf das Gerät kopiert werden, dazu unter `apk-data/files.organicmaps/` ablegen, bislang mit den Karten für Baden-Württemberg getestet:

```
apk-data/files.organicmaps/icudt70l.dat
apk-data/files.organicmaps/230210
apk-data/files.organicmaps/230210/Germany_Baden-Wurttemberg_Regierungsbezirk Karlsruhe.mwm
apk-data/files.organicmaps/230210/Germany_Baden-Wurttemberg_Regierungsbezirk Stuttgart_Stuttgart.mwm
apk-data/files.organicmaps/230210/Germany_Baden-Wurttemberg_Regierungsbezirk Stuttgart_Heilbronn.mwm
apk-data/files.organicmaps/230210/Germany_Baden-Wurttemberg_Regierungsbezirk Freiburg.mwm
```

Um die Karten herunterzuladen, organicmaps installieren, in der App gewünschte Karten laden, dann via adb herunterladen:

```bash
adb pull /storage/emulated/0/Android/data/app.organicmaps/ apk-data/files.organicmaps/
```

## install2piafmobile.sh

Skript zur Installation via adb.
* Definiert Profilnamen via `username="piafm"`
    * legt das Nutzerprofil an, das dann am Gerät eingerichtet werden muss
        * System - Erweitert/Mehrere Benutzer - piafm [nicht eingerichtet] - Nutzer einrichten (wie oben bei Ersteinrichtung)
* Installiert die apk Pakete aus adb-data/
    * diese können von einer manuellen Installation erhalten werden, z.B. mit `android-getapks.sh` Skript

```bash
$ ./install2piafmobile.sh 
settings - info - build number - 7x tippen für Entwicklermodus
settings - usb debugging (suchen) - einschalten
meldungen - usb lädt - umstellen auf Dateiübertragung
Users:
	UserInfo{0:Eigentümer:13} running
	UserInfo{11:piafm:10} running
create user piafm? [y/N]
Users:
	UserInfo{0:Eigentümer:13} running
	UserInfo{11:piafm:10} running
Ready to install software (userid=11)? [y/N]y
installing apps
installing apk-data/app.organicmaps.apk
Success
Success
[...]
copying offline maps
apk-data/files.organicmaps/: 6 files pushed. 27.1 MB/s (419804342 bytes in 14.765s)
Element einrichten
[QR code user]
[QR code password]
```
Matrix client verifizieren (mit bestehender, verifizierter Session an anderem Gerät).

## genbackground.sh

* Text zu Hintergrundbild (`background_plain.png`) hinzufügen

## Software

Aktuell wurden folgende Softwarepakete installiert:

* f-droid: alternativer Store für freie Software und zur Installation von apk aus Dateien oder von anderen Geräten
    * benötigt Rechte zur Installation von Software
* QRscanner/generator
* nextcloud: Client für Nextcloud ("BitBW-Cloud" ist Nextcloud)
* piaf.mobile: PIAF mobile
* qfield: mobiles QGIS für Feldarbeiten
* element: Chat
* kobocollect: Formulare erstellen und ausfüllen
* UAVforecast: Wetter
* firefox: Browser
* markor: Texteditor mit Markdown-Unterstützung
* keepass2android: Passwortmanager
* EmlidFlow: REACH GPS-Geräte App
* fusioninventory: Inventarisierungsclient (GLPI)
* vlc: Videoplayer


### android-getapks.sh

Holt alle Softwarepakete als apk:

```
mkdir apk-data/
pushd apk-data/
android-getapks.sh
popd
```

Nur die gewünschten Pakete nach apk-data kopieren.


Aktuelle Paketliste:

```
app.organicmaps.apk
ch.opengis.qfield.apk
com.emlid.reachview3.apk
com.nextcloud.client.apk
com.secuso.privacyFriendlyCodeScanner.apk
com.uavforecast.apk
de.proplant.piaf.mobile.apk
de.proplant.piaf_mobile_dart.apk
im.vector.app.apk
net.gsantner.markor.apk
org.fdroid.fdroid.apk
org.fusioninventory.apk
org.koboc.collect.android.apk
org.mozilla.firefox.apk
org.videolan.vlc.apk
```

    
### Screenshots

#### Nutzereinrichtung

![Nutzer einrichten](screenshots/screenshot-profile-init-01.png)

![Nutzer einrichten - weiter](screenshots/screenshot-profile-init-02.png)

![Updates](screenshots/screenshot-profile-init-updates.png)

#### Einstellungen zu USB

![Gerätename einstellen](screenshots/screenshot-settings-devicename.png)

![Build number 7x tippen für Entwickleroptionen](screenshots/screenshot-settings-build.png)

![Developer options erscheinen zusätzlich](screenshots/screenshot-settings-system-devopts.png)

![USB Einstellungen - file transfer/Dateiübertragung muss jeweils eingestellt sein](screenshots/screenshot-settings-usb-filetransfer.png)

![Abfrage auf Gerät zu USB debugging](screenshots/screenshot-usb-debugging-dialog.png)

![Meldung, dass Gerät im Laden-Modus ist (nicht: Dateitransfer)](screenshots/messages-usb-charge.png)


#### Matrix Chat Einrichtung


![Nach Anmeldung mit Credentials: Client muss verifiziert werden](screenshots/matrix-verify-alert.png)

![Nach Annahme der Verifizierung an anderem Gerät kann via QR code oder Emoji verifiziert werden](screenshots/matrix-verify-scan.png)

![Verfikation erfolgreich](screenshots/matrix-verify-successful.png)


