#!/bin/bash

for i in $(adb shell pm list packages | awk -F':' '{print $2}'); do 
    echo "---- $i ----"
    trg="$i.apk"
    if [ -e "$trg" ] ; then
        echo "skipping existing $trg"
        continue
    fi
    adb pull "$(adb shell pm path $i | awk -F':' '{print $2}')"
    mv base.apk "$trg" &> /dev/null 
done

