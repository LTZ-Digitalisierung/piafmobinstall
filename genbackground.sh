#!/bin/bash


#tmpd=$(mktemp -d --tmpdir)
tdir="apk-data/files.upload/"

if [ ! -d "$tdir" ] ; then
    "$tdir does not exist, creating..."
    mkdir -p "$tdir"
fi

for i in "$@"
do
trg="${i,,}" # lowercase
trg="apk-data/files.upload/background_${trg//[^0-9a-z-]/}.png"

echo "$trg"
convert background_plain.png -gravity North -pointsize 50 -fill '#444444' -annotate +0+20 "$i" "$trg"
done


