#!/bin/bash


#pushd $(dirname "$(realpath "$0")")

# default username and userid
username="piafm"
userid=10

function checkadbconnection(){
ok=1
while [ "$ok" != "0" ] 
do 
	adb shell whoami > /dev/null
	ok=$?
	if [ "$ok" != "0" ] 
	then
		read -r -p "ok=$ok USB Einstellungen prüfen: Dateiübertragung. weiter?"
	fi
done
}


echo "settings - info - build number - 7x tippen für Entwicklermodus"
echo "settings - usb debugging (suchen) - einschalten"
echo "meldungen - usb lädt - umstellen auf Dateiübertragung"


adb shell pm list users 
read -p "create user $username? [y/N]" ans
if [ "$ans" == "y" ] ; then
	checkadbconnection
	adb shell pm create-user "$username"
	read -p "initialisiere den user jetzt auf dem Gerät. Auf dem Gerät profil in den einstellungen anwählen und einrichten (weiter, gogle konto überspringen, privacy, muster L, abschließen). USB auf Dateiübertragung. Fertig?"
fi


checkadbconnection
adb shell pm list users 
userid=$(adb shell pm list users | grep ":$username:"| sed -e 's#UserInfo{##'| awk -F ':' '{print $1+0}')


read -p "Ready to install software (userid=$userid)? [y/N]" ans

if [ "$ans" == "y" ] ; then
echo "installing apps"

checkadbconnection

for i in apk-data/*.apk
do
	echo "installing $i"
	adb install "${i}"
	adb install --user "$userid" "${i}"
done

echo "copying offline maps"
adb push apk-data/files.organicmaps/ /storage/emulated/0/Android/data/app.organicmaps/
# TODO: find possibility to copy files to profile (currently not possible with adb)
# adb push apk-data/files.organicmaps/ "/storage/emulated/$userid/Android/data/app.organicmaps/"

fi

echo "Element einrichten"

qr "$(pass piaf.mobile/matrixuser)"

qr "$(pass piaf.mobile/matrixpw)"

